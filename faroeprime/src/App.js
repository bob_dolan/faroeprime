import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import Navbar from './components/constantComponents/Navbar';
import Footer from './components/constantComponents/Footer';

import Home from './components/Home';
import About from './components/About';
import Products from './components/products/Products';
import SignUp from './components/SignUp';
import Login from './components/Login';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />
          <Route exact path="/" component={Home} />
          <Route path="/home" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/products" component={Products} />
          <Route path="/signup" component={SignUp} />
          <Route path="/login" component={Login} />
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
