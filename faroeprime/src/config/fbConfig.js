
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Initialize Firebase
var config = {
  apiKey: "AIzaSyBRhk98xLEnXOiuPZ_ZMXiaZcmcZBHEtuE",
  authDomain: "test-project-86a26.firebaseapp.com",
  databaseURL: "https://test-project-86a26.firebaseio.com",
  projectId: "test-project-86a26",
  storageBucket: "test-project-86a26.appspot.com",
  messagingSenderId: "771980891193"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true })

export default firebase;