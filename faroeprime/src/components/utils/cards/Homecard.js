import React from 'react';

export default (props) => {

  const cardAttrs = "card hoverable " + props.color;

  return (
    <div className="col s12 m6 l4 offset-m3 center-align">
      <a href={props.link}>
        <div className={cardAttrs}>
          <div className="card-image">
            <img src={props.imgsrc} alt="" className="responsive-img"/>
            <span className="card-title black-text">{props.text}</span>
          </div>
        </div>
      </a>
    </div>
  )
}
