import React, { Component } from 'react'
import firebase from '../config/fbConfig';

export default class SignUp extends Component {

  constructor(props){
    super(props)
    this.state = {
      company_name: "",
      email: "",
      password: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event){
    let change = {}
    change[event.target.id] = event.target.value;
    this.setState(change);
  }

  handleSubmit(event){
    event.preventDefault();
    firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then(cred => {
      console.log(cred)
    });
  }

  render() {
    return (
      <div className="container">
        <h4 className="center-align grey-text flow-text">Sign up for an Faroe Prime account</h4>
        <p className="center-align grey-text flow-text">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore beatae, repellendus sint inventore magni distinctio eveniet vero officiis doloremque tempora officia ratione iste cupiditate perspiciatis blanditiis aliquid. Recusandae, eaque eligendi!</p>

        <form className="col s12">
          <div className="row">
            <div className="input-field">
              <i className="material-icons prefix">account_circle</i>
              <input id="company_name" type="text" className="validate" onChange={this.handleChange} value={this.state.company_name}/>
              <label htmlFor="company_name">Company Name</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field">
              <i className="material-icons prefix">email</i>
              <input id="email" type="email" className="validate" onChange={this.handleChange} value={this.state.email}/>
              <label htmlFor="email">Email</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field">
              <i className="material-icons prefix">lock</i>
              <input id="password" type="password" className="validate" onChange={this.handleChange} value={this.state.password}/>
              <label htmlFor="password">Password</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field">
              <i className="material-icons prefix">lock</i>
              <input id="confirm_password" type="password" className="validate"/>
              <label htmlFor="confirm_password">Confirm Password</label>
            </div>
          </div>

          <div className="row center-align">
            <button className="btn waves-effect waves-light green" type="submit" name="action" onClick={this.handleSubmit}>
              Sign up
              <i className="material-icons right">send</i>
            </button>
          </div>
        </form>
      </div>
    )
  }
}
