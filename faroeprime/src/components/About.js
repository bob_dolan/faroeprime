import React, { Component } from 'react'

export default class About extends Component {

  render() {
    return (
      <div className="container" >
        <h4 className="white-text center-align">About</h4>
        <p className="white-text flow-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum praesentium repellat ipsum. Blanditiis commodi consequatur eos enim quo, voluptatibus, labore odit quae impedit eum ullam nulla corrupti accusamus ipsum. Est!</p>
      </div>
    )
  }
}
