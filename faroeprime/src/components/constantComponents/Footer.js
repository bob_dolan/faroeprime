import React from 'react'

export default () => {

  const socialLinks = ['Facebook', 'Twitter', 'Linked In', 'Instagram'];

  let links = socialLinks.map((link, index) => {
    return <li key={index}><a href="/" className="grey-text text-lighten-3">{link}</a></li>
  })

  return (
    <footer className="page-footer grey darken-3">
      <div className="container">
        <div className="row">
          <div className="col l6 s12">
            <h5>About Us</h5>
              <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Earum ad blanditiis eum tempora esse fugit et natus in nam repellendus ex dolore, minima, placeat perspiciatis nihil laboriosam asperiores sit itaque.</p>
          </div>
          <div className="col l4 offset-l2 s12">
            <h5 className="white-text">Connect</h5>
            <ul>
              {links}
            </ul>
          </div>
        </div>
      </div>

      <div className="footer-copyright grey darken-4">
        <div className="container center-align">&copy; 2018 Faroe Prime</div>
      </div>

    </footer>
  )
}
