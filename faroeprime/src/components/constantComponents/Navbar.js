import React, { Component } from 'react';

export default class Navbar extends Component {
  render() {
    return (
    <header>
        <nav className="nav-wrapper transparent">
          <div className="container">
            <a href="/" className="brand-logo">
              <img src="logo_package/PNG/bogmærke_farve.png" alt="" id="brand-logo"/>
            </a>
            
            <a href="#!" className="sidenav-trigger" data-target="mobile-menu">
              <i className="material-icons">menu</i>
            </a>

            <ul className="right hide-on-med-and-down">
              <li><a href="/about">About</a></li>
              <li><a href="/products">Products</a></li>
              <li><a href="/signup">Sign Up</a></li>
              <li><a href="/login">Login</a></li>
            </ul>

            <ul className="sidenav grey" id="mobile-menu">
              <li><a href="/about">About</a></li>
              <li><a href="/products">Products</a></li>
              <li><a href="/signup">Sign Up</a></li>
              <li><a href="/login">Login</a></li>
            </ul>

          </div>
        </nav>
      </header>
    )
  }
}
