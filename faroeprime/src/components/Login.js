import React, { Component } from 'react'

export default class SignUp extends Component {

  constructor(props){
    super(props)
    this.state = {
      email: '',
      password: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event){
    let change = {}
    change[event.target.id] = event.target.value;
    this.setState(change);
  }

  handleSubmit(event){
    event.preventDefault();
    console.log(this.state);
  }

  render() {
    return (
      <div className="container">
        <h4 className="center-align grey-text flow-text">Login</h4>
        <p className="center-align grey-text flow-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus voluptates facere temporibus tenetur, at iste eos! Maxime aspernatur libero sunt molestias dicta atque dolorem voluptate cupiditate, eum temporibus, cumque sequi.</p>
        
        <form className="col s12">
          <div className="row">
            <div className="input-field">
              <i className="material-icons prefix">email</i>
              <input id="email" type="email" className="validate" onChange={this.handleChange} value={this.state.email} />
              <label htmlFor="email">Email</label>
              <span className="helper-text" data-error="Invalid format..."></span>
            </div>
          </div>

          <div className="row">
            <div className="input-field">
              <i className="material-icons prefix">lock</i>
              <input id="password" type="password" className="validate" onChange={this.handleChange} value={this.state.password}/>
              <label htmlFor="password">Password</label>
            </div>
          </div>

          <div className="row center-align">
            <button className="btn waves-effect waves-light green" type="submit" name="action" onClick={this.handleSubmit}>
              Login
              <i className="material-icons right">send</i>
            </button>
          </div>
        </form>
      </div>
    )
  }
}
