import React from 'react'

export default () => {
  return (
    <div className="container">
      <div className="collection with-header">
        <li className="collection-header center grey darken-2">
          <h4 className="white-text">Products</h4>
        </li>
        
        <a href="/products" className="collection-item avatar black-text grey darken-1">
          <img src="fishimgs/Salmon.jpg" alt="" className="responsive-img circle"/>
          <span className="title">Laks</span>
          <p>First line <br />
            second line
          </p>
        </a>

        <a href="/products" className="collection-item avatar black-text grey darken-1">
          <img src="fishimgs/Salmon.jpg" alt="" className="responsive-img circle"/>
          <span className="title">Laks</span>
          <p>First line <br />
            second line
          </p>
        </a>

        <a href="/products" className="collection-item avatar black-text grey darken-1">
          <img src="fishimgs/Salmon.jpg" alt="" className="responsive-img circle"/>
          <span className="title">Laks</span>
          <p>First line <br />
            second line
          </p>
        </a>

        <a href="/products" className="collection-item avatar black-text grey darken-1">
          <img src="fishimgs/Salmon.jpg" alt="" className="responsive-img circle"/>
          <span className="title">Laks</span>
          <p>First line <br />
            second line
          </p>
        </a>
      </div>
    </div>
  )
}
