import React, { Component } from 'react'

import Card from './utils/cards/Homecard';

export default class Home extends Component {
  render() {
    return (
      <div>
        <div className="container valign-wrapper">
          <div className="row">

            <Card 
              color="blue darken-4"
              imgsrc="logo_package/PNG/bogmærke_sort.png"
              text="Where am I ?"
              link="/about"
            />
            
            <Card 
              color="white"
              imgsrc="logo_package/PNG/bogmærke_farve.png"
              text="Sign up"
              link="/signup"
            />
            
            <Card 
              color="red darken-4"
              imgsrc="logo_package/PNG/bogmærke_sort.png"
              text="Login"
              link="/login"
            />
          </div>
        </div>
      </div>
    )
  }
}
